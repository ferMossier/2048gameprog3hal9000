package Logica;

public class Tablero
{
	private Casillero[][] valoresCasillas;

	public Tablero()
	{

		this.valoresCasillas=new Casillero[4][4];
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				valoresCasillas[i][j]=new Casillero();
			}
		}

		// inicializamos las casillas en cero en una matriz de 4x4
		// IDEA: Probablemente sea buena idea crear la clase "Casillero" y que el tablero este formado por "Casilleros", 
		// donde cada uno guarda su propio valor y puede consultar por sus casilleros adyacentes.
	}

	private boolean verificarCasilla(int x, int y)
	{
		return this.valoresCasillas[x][y].estaVacia();
			
	}

	public void agregarValor() 
	{
		int posicionX = getRandomPosition();
		int posicionY = getRandomPosition();
		while (!verificarCasilla(posicionX, posicionY)) {
			posicionX = getRandomPosition();
			posicionY = getRandomPosition();
		}
		this.valoresCasillas[posicionX][posicionY].agregarValor(getTwoOrFour());
		
	}


	public void inicializarTablero() //Faltan verificaciones
	{
		this.valoresCasillas[getRandomPosition()][getRandomPosition()].agregarValor(getTwoOrFour());
		this.valoresCasillas[getRandomPosition()][getRandomPosition()].agregarValor(getTwoOrFour());
	}

	private int getRandomNumber()
	{
		double rand = Math.random() * 10;
		return (int) rand;
	}

	private int getTwoOrFour()
	{
		int rand = getRandomNumber();
		if (rand < 5)
			return 2;
		return 4;
	}

	private int getRandomPosition()
	{ 
		int rand = getRandomNumber();
		return rand%4;//devuelve la congruencia módulo 4 de rand
	}

	public void moverTodasDerecha() {
		this.agrupaDerecha();
		for(int fila=0; fila<=3;fila++) {
		for (int columna = 3 ; columna > 0;  columna--) {
			Casillero casilla = this.valoresCasillas[fila][columna];
			Casillero casilleroAdy = this.valoresCasillas[fila][columna-1];
			int valorCa = valorCasillero (casilla);
			int valorCaAdy = valorCasillero (casilleroAdy);
			if (valorCa == valorCaAdy) {
			this.valoresCasillas[fila][columna].suma();  // multiplica el valor de la casilla, ya que solo suma cuando son iguales
			this.valoresCasillas[fila][columna-1].agregarValor(0); // colocamos 0 en la casilla adyacente 
			 }
			}
		}
	}
	
	
	// busco el valor del objeto Casillero 
	public int valorCasillero (Casillero casillero) {
		return casillero.muestraValor();
	}
	
	
	public void agrupaDerecha() {
		for(int i=0;i<4;i++) {
			Casillero [] aux = new Casillero [4];
			aux[0]=new Casillero();
			aux[1]=new Casillero();
			aux[2]=new Casillero();
			aux[3]=new Casillero();
			int indiceVecAux = 3;
			for(int j=3; j>=0; j--) {
				if (this.valoresCasillas[i][j].muestraValor() != 0) {
					aux[indiceVecAux] = this.valoresCasillas[i][j];
					indiceVecAux--;
				}
			}
			this.valoresCasillas[i]=aux;
		}
	}
	
	public void moverTodasIzquierda() {
		this.agrupaIzquierda();
		for(int fila=0; fila<=3;fila++) {
		for (int columna = 0 ; columna<3;  columna++) {
			Casillero casilla = this.valoresCasillas[fila][columna];
			Casillero casilleroAdy = this.valoresCasillas[fila][columna+1];
			int valorCa = valorCasillero (casilla);
			int valorCaAdy = valorCasillero (casilleroAdy);
			if (valorCa == valorCaAdy) {
			this.valoresCasillas[fila][columna].suma();  // multiplica el valor de la casilla, ya que solo suma cuando son iguales
			this.valoresCasillas[fila][columna+1].agregarValor(0); // colocamos 0 en la casilla adyacente
				 }
			}
		}
	
	}
	
	
	public void agrupaIzquierda() {
		for(int i=0;i<4;i++) {
			Casillero [] aux = new Casillero [4];
			aux[0]=new Casillero();
			aux[1]=new Casillero();
			aux[2]=new Casillero();
			aux[3]=new Casillero();
			int indiceVecAux = 0;
			for(int j=0; j<=3; j++) {
				if (this.valoresCasillas[i][j].muestraValor() != 0) {
					aux[indiceVecAux] = this.valoresCasillas[i][j];
					indiceVecAux++;
				}
			}
			this.valoresCasillas[i]=aux;
		}
	}
	
	public void moverTodasAbajo() {
		this.agrupaAbajo();
		for(int columna=0; columna<=3;columna++) {
			for (int fila = 3 ; fila > 0;  fila--) {
				Casillero casilla = this.valoresCasillas[fila][columna];
				Casillero casilleroAdy = this.valoresCasillas[fila -1][columna];
				int valorCa = valorCasillero (casilla);
				int valorCaAdy = valorCasillero (casilleroAdy);
				if (valorCa == valorCaAdy) {
				this.valoresCasillas[fila][columna].suma();  // multiplica el valor de la casilla, ya que solo suma cuando son iguales
				this.valoresCasillas[fila-1][columna].agregarValor(0); // colocamos 0 en la casilla adyacente 
				 }
				}
			}
	}
	public void agrupaAbajo() {
		transponer();
		agrupaDerecha();
		transponer();
	}
	
	
	public void moverTodasArriba() {
		this.agrupaArriba();
		for(int columna=0; columna<=3;columna++) {
		for (int fila = 0 ; fila<3;  fila++) {
			Casillero casilla = this.valoresCasillas[fila][columna];
			Casillero casilleroAdy = this.valoresCasillas[fila+1][columna];
			int valorCa = valorCasillero (casilla);
			int valorCaAdy = valorCasillero (casilleroAdy);
			if (valorCa == valorCaAdy) {
			this.valoresCasillas[fila][columna].suma();  // multiplica el valor de la casilla, ya que solo suma cuando son iguales
			this.valoresCasillas[fila+1][columna].agregarValor(0); // colocamos 0 en la casilla adyacente
				 }
			}
		}
	
	}
	
	public void agrupaArriba() {
		transponer();
		agrupaIzquierda();
		transponer();
	}
	
	//Transponer convierte filas en columnas. (Transposición de matriz)
	public void transponer() {
		Casillero [][] aux = new Casillero [4][4];
		for(int i = 0; i<4; i++) {
			for(int j = 0; j<4; j++) {
				aux[i][j] = new Casillero();
			}
		}
		for(int i = 0; i<4;i++) {
			for(int j = 0; j< 4; j++) {
				aux[j][i].agregarValor(this.valoresCasillas[i][j].muestraValor());
			}
		}
		this.valoresCasillas = aux;
	}
	
	

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < valoresCasillas[0].length; i++)
		{
			for (int j = 0; j < valoresCasillas.length; j++)
			{
				sb.append(valoresCasillas[i][j].toString() + "   ");
			}
			sb.append("\n\n");
		}
		return sb.toString();
	}

}
