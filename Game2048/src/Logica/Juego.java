package Logica;

public class Juego
{
	// Variables de instancia:
	// . .

	private Tablero tablero = new Tablero(); 

	// Constructor
	public Juego()
	{
		tablero.inicializarTablero();
	}

	// Metodos

	public String verTablero() {
		return tablero.toString();
	}
	
	public void agregarValorEnTablero() {
		tablero.agregarValor();
	}
	
	public void moverDerecha() {
		this.tablero.moverTodasDerecha();
	}

	
	public void agruparDer() {
		this.tablero.agrupaDerecha();
	}
	
	public void agruparIzq() {
		this.tablero.agrupaIzquierda();
	}
	
	public void agruparAb() {
		this.tablero.agrupaAbajo();
	}
	
	public void agruparArr() {
		this.tablero.agrupaArriba();
	}
	public void moverIzquierda() {
		this.tablero.moverTodasIzquierda();
	}
	public void moverArriba() {
		this.tablero.moverTodasArriba();
	}
	public void moverAbajo() {
		this.tablero.moverTodasAbajo();
	}
}





















