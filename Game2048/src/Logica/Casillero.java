package Logica;

public class Casillero {
	
	private int valor;
	
	public Casillero() {
		this.valor=0;
		}
	public int muestraValor() {
		return this.valor;
	}
	public boolean contieneValor(int i) {
		return this.valor == i;
	}
	
	public void agregarValor(int numero) {
		if(numero==2 || numero==4 || numero==0)
			valor=numero;
	}
	
	public boolean estaVacia() {
		return this.valor==0?true:false;
	}
	
	public void suma() {
		this.valor=this.valor*2;
	}
	@Override
	public String toString() {
		return "" + this.muestraValor();
	}

	

}

